const User = require('../models/User');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');
import {ResponseBody} from './Response';

const UserController = () => {
  const register = async (req, res) => {
    let respBody = new ResponseBody(false);
    const { body } = req;
    const {password, email, name} = body;
    if(name && email && password){
      try {
        const user = await User.create({
          email, password, name
        });
        const token = authService().issue({ id: user.id });
        respBody.setData({
          token, user
        });
        respBody.setSuccess();
        return res.status(200).json(respBody.getResponse());
      } catch (err) {
        console.log(err);
        respBody.setData('INTERNAL_ERROR');
        return res.status(500).json(respBody.getResponse());
      }
    }
    return res.status(400).json(respBody.getResponse());
  };

  const login = async (req, res) => {
    let respBody = new ResponseBody(false);
    const { email, password } = req.body;

    if (email && password) {
      try {
        const user = await User
          .findOne({
            where: {
              email,
            },
          });

        if (!user) {
          respBody.setData('INVALID_USER');
          return res.status(400).json(respBody.getResponse());
        }

        if (bcryptService().comparePassword(password, user.password)) {
          const token = authService().issue({ id: user.id });
          respBody.setData({
            token, user
          });
          respBody.setSuccess();
          return res.status(200).json(respBody.getResponse());
        }
        respBody.setData('UNAUTHORIZED');
        return res.status(401).json(respBody.getResponse());
      } catch (err) {
        console.log(err);
        respBody.setData('INTERNAL_ERROR');
        return res.status(500).json(respBody.getResponse());
      }
    }
    respBody.setData('BAD_REQUEST');
    return res.status(400).json(respBody.getResponse());
  };

  const validate = (req, res) => {
    let respBody = new ResponseBody(false);
    const { token } = req.body;

    authService().verify(token, (err) => {
      if (err) {
        respBody.setData("INVALID_TOKEN");
        respBody.setSuccess(false);
        return res.status(401).json(respBody.getResponse());
      }
      respBody.setData("VALID_TOKEN");
      respBody.setSuccess();
      return res.status(200).json(respBody.getResponse());
    });
    return res.status(400).json(respBody.getResponse());
  };

  const getAll = async (req, res) => {
    let respBody = new ResponseBody(false);
    try {
      const users = await User.findAll();
      respBody.setData(users);
      respBody.setSuccess();
      return res.status(200).json(respBody.getResponse());
    } catch (err) {
      console.log(err);
      respBody.setData('UNAUTHORIZED');
      return res.status(500).json(respBody.getResponse());
    }
  };


  return {
    register,
    login,
    validate,
    getAll,
  };
};

module.exports = UserController;
