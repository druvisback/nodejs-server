const User = require('../models/User');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');

const TestController = () => {
  const hello = async (req, res) => {
    const { body } = req;
    return res.status(200).json({ msg: 'Hello' });
  };


  return {
    hello,
  };
};

module.exports = TestController;
