const express = require('express');
const router = express.Router();
const swig  = require('swig');


// Home page route.
router.get('/', function (req, res) {
    const template = swig.compileFile('./server/views/home.html');
    const output = template({
        pagename: 'awesome people',
        authors: ['Paul', 'Jim', 'Jane']
    });
    res.send(output);
});

// About page route.
router.get('/about', function (req, res) {
    res.send('About this wiki');
});

module.exports = router;
