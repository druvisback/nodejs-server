require = require('esm')(module)

const express = require("express");
const consolidate = require('consolidate');
const path = require('path');
const server_router = require('./server/routes/web')
const port = parseInt(process.env.PORT, 10) || 3002;
const environment = process.env.NODE_ENV;

const bodyParser = require('body-parser');
const helmet = require('helmet');
const http = require('http');
const socketIo = require('socket.io')(http);
const mapRoutes = require('express-routes-mapper');
const cors = require('cors');

const config = require('./server/config');
const dbService = require('./server/http/services/db.service');
const auth = require('./server/http/policies/auth.policy');

function createServer() {
    const app = express();
    const mappedOpenRoutes = mapRoutes(config.publicRoutes, 'server/http/controllers/');
    const mappedAuthRoutes = mapRoutes(config.privateRoutes, 'server/http/controllers/');
    const server = http.Server(app);
    const DB = dbService(environment, config.migrate).start();

    // allow cross origin requests
    // configure to only allow requests from certain origins
    const whitelist = ['http://localhost:8080'];
    const corsOptions = {
        origin: function (origin, callback) {
            if (true || whitelist.indexOf(origin) !== -1) {
                callback(null, true)
            } else {
                callback(new Error('Not allowed by CORS'))
            }
        },
        credentials: true
    }
    app.use(cors(corsOptions));

    // secure express app
    app.use(helmet({
        dnsPrefetchControl: false,
        frameguard: false,
        ieNoOpen: false,
    }));

    // parsing the request bodys
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());


    //Swig Template Engine...


    // view engine setup
    app.engine('html', consolidate.swig)
    app.set('views', path.join(__dirname, 'server/views'));
    app.set('view engine', 'html');

    // secure your private routes with jwt authentication middleware
    app.all('/auth/*', (req, res, next) => auth(req, res, next));

    // fill routes for express application
    app.use('/api/auth', mappedAuthRoutes);
    app.use('/api', mappedOpenRoutes);


    app.use('/', server_router);

    return app;
}


try {
    const app = createServer();
    const server = app.listen(port, () => {
        if (environment !== 'production' &&
            environment !== 'development' &&
            environment !== 'testing'
        ) {
            console.error(`NODE_ENV is set to ${environment}, but only production and development are valid.`);
            process.exit(1);
        }
    });

    let activeSockets = [];
    const getUserFromSocketId = (socketId)=>{
           const users = activeSockets.filter(u=>u.id===socketId);
           if(users.length){
               return users[0];
           }else{
               return false;
           }
    };
    /*socketIo.listen(server);
    socketIo.on('connection', function (socket) {
        console.log("SOCKET: User connected...");

        const existingSocket = activeSockets.find(
            existingSocket => existingSocket === socket.id
        );

        if (!existingSocket) {
            //activeSockets.push({id: socket.id, name: 'Unknown'});
            socket.emit("update-user-list", {
                users: activeSockets.filter(
                    existingSocket => existingSocket.id !== socket.id
                )
            });

            socket.broadcast.emit("update-user-list", {
                users: activeSockets
            });
        }

        socket.on('client_registration', function(data){
            let existSocket = activeSockets.filter(s=>{
                return s.id === data.id;
            }).length > 0;
            if(existSocket){
                activeSockets = activeSockets.map(s=>{
                    if(s.id === data.id){
                        s.name = data.name;
                    }
                    return s;
                })
            }else{
                activeSockets.push({id: data.id, name: data.name});
            }
            socket.broadcast.emit("update-user-list", {
                users: activeSockets
            });
            console.log({activeSockets});
        });
        socket.on("call-user", data => {
            const {offer, to} = data;
            socket.to(to.id).emit("call-made", {
                offer,
                from: getUserFromSocketId(socket.id)
            });
        });
        socket.on("feedback-user", data => {
            const {offer, to} = data;
            socket.to(to.id).emit("receiving-feedback", {
                offer,
                from: getUserFromSocketId(socket.id)
            });
        });
        socket.on("make-answer", data => {
            const {answer, to, feedback} = data;
            console.log("Make answer => ",{feedback});
            socket.to(data.to.id).emit("answer-made", {
                from: getUserFromSocketId(socket.id),
                answer,
                feedback
            });
        });
        socket.on('disconnect', () => {
            console.log('SOCKET: User Disconnected...');
            activeSockets = activeSockets.filter(
                existingSocket => existingSocket.id !== socket.id
            );
            socket.broadcast.emit("remove-user", {
                id: socket.id
            });
        });
    });*/

    switch (environment) {
        case 'development':
            console.log("Development Build Running at port(" + port + ")...");
            break;
        case 'production':
            console.log("Production Build Running at port(" + port + ")...");
            break;
        case 'testing':
            console.log("Testing Build Running at port(" + port + ")...");
            break;
    }
}catch(ex){
    console.error(ex.stack);
    process.exit(1)
};
